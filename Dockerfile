FROM ubuntu

RUN apt-get update \
    && apt install -y build-essential zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/lib/jvm
ADD https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-21.2.0/graalvm-ce-java16-linux-amd64-21.2.0.tar.gz ./graalvm-ce-java16-linux-amd64-21.2.0.tar.gz
RUN tar -xf graalvm-ce-java16-linux-amd64-21.2.0.tar.gz && rm graalvm-ce-java16-linux-amd64-21.2.0.tar.gz

ENV JAVA_HOME="/usr/lib/jvm/graalvm-ce-java16-21.2.0"
ENV PATH="${PATH}:${JAVA_HOME}/bin"

ADD https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-21.2.0/native-image-installable-svm-java16-linux-amd64-21.2.0.jar ./native-image-installable-svm-java16-linux-amd64-21.2.0.jar
RUN gu -L install native-image-installable-svm-java16-linux-amd64-21.2.0.jar